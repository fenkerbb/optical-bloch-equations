// Author: Benjamin Fenker
#include <iostream>
#include <string>
#include <vector>

#include <exception>
#include <fstream>
#include <iostream>
#include <math.h>
#include <string>

// ROOT includes
#include <TH1D.h>

// boost includes
#include <boost/algorithm/string.hpp>

#include "units.h"
#include "OPfluorFitting.h"
enum {
  success = 0,
  bad_file = 1,
  read_error = 2,
};

double pi_weight[8] = {1., 1., 1., 1., 1., 1., 1., 1.};

int OPFit::ReadPhotoionWeights(std::string fname) {
  std::ifstream ifs(fname, std::ifstream::in);
  if (!ifs.is_open()) {
    std::cout << "Could not find PI file " << fname << ". Equal weights used.";
    std::cout << " (Enter to confirm)" << std::endl;
    int j;
    std::cin >> j;
    return bad_file;
  }

  std::string line;
  std::vector<std::string> word;
  while (std::getline(ifs, line)) {
    while (line.c_str()[0] == ' ') line.erase(0, 1);
    std::cout << line << std::endl;
    boost::split(word, line, boost::is_any_of(" \t"));
  }
  int zz = 0;
  try {
    std::cout << "New PI weights: " << std::endl;
    for (int i = 0; i < 8; i++) {
      pi_weight[i] = stod(word[i]);
      zz = i;
      std::cout << pi_weight[i] << "\t";
    }
  }
  catch (std::invalid_argument &exc) {
    std::cout << "Invalid argument" << std::endl;
    std::cout << exc.what() << std::endl;
    std::cout << "Error converting " << pi_weight[zz] << " to double in file "
              << fname << std::endl;
    return read_error;
  }
  catch (std::out_of_range) {
    std::cout << "Out of range" << std::endl;
    std::cout << "Error converting " << pi_weight[zz] << " to double in file "
              << fname << std::endl;
    return read_error;
  }

  std::cout << std::endl;
  return success;
}

int OPFit::FillFluorHistFromFile(TH1D *hist_to_fill, std::string fname,
                                 double tscale, std::string option,
                                 double toffset) {
  boost::algorithm::to_lower(option);

  std::ifstream ifs(fname, std::ifstream::in);
  if (!ifs.is_open()) {
    return bad_file;
  }

  
  std::string line;
  std::vector<std::string> word;
  std::vector<int> times_filled(hist_to_fill -> GetNbinsX() + 1, 0);
  for (int i = 1; i <= hist_to_fill -> GetNbinsX(); i++) 
    hist_to_fill -> SetBinContent(i, 0.0);

  while (std::getline(ifs, line)) {
    while (line.c_str()[0] == ' ') line.erase(0, 1);
    //    std::cout << line << std::endl;
    boost::split(word, line, boost::is_any_of(" \t"));
    //    std::cout << word[0] << "\t" << word[word.size()-1] << std::endl;
    double time, fluor;
    try {
      time = tscale * stod(word[0]);
      //      fluor = stod(word[word.size()-1]);
      double es[8];
      double sum = 0.;
      for (int e = 0; e < 8; e++) {
        es[e] = pi_weight[e]*(stod(word[9+e]));
        sum = sum + es[e];
      }
      //      if (sum > 0.0002) std::cout << sum << "\t" << stod(word[word.size()-1]) << std::endl;
      fluor = sum;
    }
    catch (...) {
      std::cout << "Error converting to double in file " << fname << std::endl;
      return read_error;
    }
    // Adjust time
    time = time + toffset;
    double bw = hist_to_fill -> GetBinWidth(hist_to_fill -> FindBin(time));
    if (ex_max > ex_min && time  + bw >= ex_min && time <= ex_max) continue;

    //    std::cout << time << "\t" << fluor << std::endl;
    int binn = hist_to_fill -> FindBin(time);
    hist_to_fill ->
        SetBinContent(binn, hist_to_fill->GetBinContent(binn) + fluor);
    times_filled[binn]++;
  }
  ifs.close();
  for (int i = 1; i <= hist_to_fill -> GetNbinsX(); i++) {
    if (times_filled[i] > 0) {
      hist_to_fill ->
          SetBinContent(i, hist_to_fill -> GetBinContent(i) / times_filled[i]);
      // std::cout << "DOUBLING" << std::endl;
      // hist_to_fill -> SetBinContent(i, hist_to_fill->GetBinContent(i)*2.0);

    } else {
      hist_to_fill -> SetBinContent(i, 0.0);
    }
    //    hist_to_fill -> SetBinContent(i, hist_to_fill -> GetBinContent(i));
    hist_to_fill -> SetBinError(i, 0.0);
    if (option == "rate") {
      hist_to_fill -> SetBinContent(i, hist_to_fill -> GetBinContent(i) / 
                                    hist_to_fill -> GetBinWidth(i));
      // hist_to_fill -> SetBinError(i, hist_to_fill -> GetBinError(i) /
      //                             hist_to_fill -> GetBinWidth(i));
    }
  }
  
  return success;
}

TH1D* OPFit::GetResidualHistogram(TH1D *data, TH1D *model,
                                  double min, double max) {
  if (!HistsHaveSameBinning(data, model)) {
    std::cout << "ERROR: CANNOT COMPARE HISTS " << data -> GetName() << " and ";
    std::cout << model -> GetName() << " MUST RETURN 0.0 CHI2" << std::endl;
    return NULL;
  }
  TH1D *residuals = new TH1D("residuals", "residuals", data -> GetNbinsX(),
                             data -> GetBinLowEdge(1),
                             data -> GetBinLowEdge(data -> GetNbinsX()) +
                             data -> GetBinWidth(data -> GetNbinsX()));
  for (int i = 0; i <= data -> GetNbinsX(); i++) {
    double time = data -> GetBinLowEdge(i);
    if (ex_max > ex_min && time > ex_min && time < ex_max) continue;
      if (data -> GetBinLowEdge(i) < min) continue;
      if (data -> GetBinLowEdge(i) + data -> GetBinWidth(i) > max) continue;

      double sim = model -> GetBinContent(i);
      double exp = data -> GetBinContent(i);
      double r = 0.0;
      if (sim > 0) {
        r = (exp - sim) / sqrt(sim);
      }
      residuals -> SetBinContent(i, r);
      residuals -> SetBinError(i, 0.0);
  }
  return residuals;
}

double OPFit::CompareFluorHists(TH1D *data, TH1D *model, std::string option,
                                double min, double max) {
  int verbose = 0;
  if (verbose > 0) {
    std::cout << "Center\tSimulation\tData\tUncert.\tchi2" << std::endl;
  }
  boost::algorithm::to_lower(option);
  if (!HistsHaveSameBinning(data, model)) {
    std::cout << "ERROR: CANNOT COMPARE HISTS " << data -> GetName() << " and ";
    std::cout << model -> GetName() << " MUST RETURN 0.0 CHI2" << std::endl;
    return 0.0;
  }
  if (option == "logl") {
    //    std::cout << "Fitting logl" << std::endl;
    double logl = 0.0;
    double t = 0.0;
    for (int i = 0; i <= data -> GetNbinsX(); i++) {
      double time = data -> GetBinLowEdge(i);
      double bw = data -> GetBinWidth(i);
      //      std::cout << time << "\t" << bw << std::endl;
      if (time < min) continue;
      if (time + bw > max) continue;
      if (ex_max > ex_min && (time + bw >= ex_min && time <= ex_max)) continue;
      //      std::cout << "PASSED!" << std::endl;
      double sim = model -> GetBinContent(i);
      double exp = data -> GetBinContent(i);

      if (sim > 0) {
        if (exp > 0) {
          t = (sim - exp + exp*log(exp / sim));
        } else {                        // exp <= 0
          t = sim;
        }
      } else {                          //  sim < 0
        t = 0.0;
      }
      if (verbose > 0) {
        std::cout << data -> GetBinCenter(i) << "\t" << sim << "\t"
                  << exp << "\t" << t << std::endl;
      }

      // Multiply by two to get error definitions right
      t = t * 2.0;

      logl = logl + t;
    }
    int j;
    if (verbose > 0) {
      std::cout << "Enter any number to continue...";
      std::cin >> j;
    }
    return logl;
  } else {
    double chi2 = 0.0;
    double t = 0.0;
    for (int i = 0; i <= data -> GetNbinsX(); i++) {
      
      if (data -> GetBinLowEdge(i) < min) continue;
      if (data -> GetBinLowEdge(i) + data -> GetBinWidth(i) > max) continue;
      double time = data -> GetBinLowEdge(i);
      if (ex_max > ex_min && time > ex_min && time < ex_max) continue;

      double sim = model -> GetBinContent(i);
      double exp = data -> GetBinContent(i);
      double err = data -> GetBinError(i);

      t = exp - sim;
      if (err > 0.0) {
        t = t / (err*err);              // squared right? of course!
      }
      
      t = t * t;
      if (verbose > 0) {
        std::cout << data -> GetBinCenter(i) << "\t" << sim << "\t"
                  << exp << "\t" << err << "\t" << t << std::endl;
      }
      chi2 = chi2 + t;
    }
    return chi2;
  }
}

bool OPFit::HistsHaveSameBinning(TH1D *a, TH1D *b) {
  bool same = true;
  double eps = 1.E-3;
  if (a -> GetNbinsX() != b -> GetNbinsX()) same = false;

  if (same) {
    for (int i = 1; i <= a -> GetNbinsX(); i++) {
      if (fabs(a->GetBinCenter(i) - b->GetBinCenter(i)) > eps) same = false;
    }
  }

  return same;
}

void OPFit::ScaleSimulationIncludingBackground(TH1D *data, TH1D *model,
                                               double bkg_per_bin, double start,
                                               double stop) {
  int verbose = 0;
  double scale_num = (data -> Integral(data -> FindBin(start),
                                       data -> FindBin(stop)));
  int nbins = data -> FindBin(stop) - data -> FindBin(start);
  double scale_den = model -> Integral(model -> FindBin(start),
                                       model -> FindBin(stop));
  if (verbose > 1) {
    std::cout << "Numerator from " << start << " to " << stop << " = "
              << scale_num << std::endl;
    std::cout << "Denominator from " << start << " to " << stop << " = "
              << scale_den << std::endl;
    
    std::cout << "Bin containing stop " << stop << "\t"
              << data -> FindBin(stop) << std::endl;
  }
  if (ex_max > ex_min) {
    nbins = nbins - (data -> FindBin(ex_max) - data -> FindBin(ex_min));
    scale_num = scale_num - (data -> Integral(data -> FindBin(ex_min),
                                              data -> FindBin(ex_max)));
    scale_den = scale_den - (model -> Integral(model -> FindBin(ex_min),
                                               model -> FindBin(ex_max)));
    if (verbose > 1) {
      std::cout << "Numerator exclude " << ex_min << " to " << ex_max << " = "
                << (data -> Integral(data -> FindBin(ex_min),
                                     data -> FindBin(ex_max)))
                << std::endl;
      std::cout << "Denominator exclude " << ex_min << " to " << ex_max << " = "
                << (model -> Integral(model -> FindBin(ex_min),
                                      model -> FindBin(ex_max)))
                << std::endl;
    }
  }

  // std::cout << "In the data there are " << scale_num << " counts in "
  //           << nbins << " bins, bin width: " << data -> GetBinWidth(5)
  //           << " from " << start << " --> " << stop << std::endl;
      

  scale_num = scale_num - (bkg_per_bin * nbins);

  double scale = scale_num / scale_den;
  //  scale = 2633.;
  model -> Scale(scale);
  if (verbose > 0) {
    std::cout << "Scaling by " << scale << " and adding bkg = "
              << bkg_per_bin << std::endl;
  }
  for (int i = 1; i < model -> GetNbinsX(); i++) {
    double time = model -> GetBinLowEdge(i);
    if (ex_max > ex_min && time > ex_min && time < ex_max) continue;    

    model -> SetBinContent(i, model -> GetBinContent(i) + bkg_per_bin);
  }

}

void OPFit::ScaleSimulationIncludingSNratio(TH1D *data, TH1D *model,
                                            double sn_ratio, double start,
                                            double stop) {
  int verbose = 0;
  int nbins = data -> FindBin(stop) - data -> FindBin(start);
  double ntot = (data -> Integral(data -> FindBin(start),
                                  data -> FindBin(stop)));
  if (verbose > 1) {
    std::cout << "Bin containing start " << start << "\t"
              << data -> FindBin(start) << std::endl;
    std::cout << "Bin containing stop " << stop << "\t"
              << data -> FindBin(stop) << std::endl;
  }
  if (ex_max > ex_min) {
    nbins = nbins - (data -> FindBin(ex_max) - data -> FindBin(ex_min));
    ntot = ntot - (data -> Integral(data -> FindBin(ex_min),
                                    data -> FindBin(ex_max)));
    if (verbose > 1) {
      std::cout << "Excluding start " << ex_min << "\t" << data -> FindBin(ex_min)
                << std::endl;
      std::cout << "Excluding stop " << ex_max << "\t" << data -> FindBin(ex_max)
                << std::endl;
    }
  }

  double bkg_per_bin = (ntot / nbins) / (sn_ratio + 1.0);
  if (verbose > 0) {
    std::cout << "Ntot: " << ntot << " nbins: " << nbins << " sn_ratio: "
              << sn_ratio << " bkg_per_bin: " << bkg_per_bin << std::endl;
  }
    
  ScaleSimulationIncludingBackground(data, model, bkg_per_bin, start, stop);
}

double OPFit::GetFinalPolarizationFromFile(std::string fname, double start,
                                           double stop) {
  return GetFinalValueFromFile(3, fname, start, stop);
}

double OPFit::GetFinalAlignmentFromFile(std::string fname, double start,
                                        double stop) {
  return GetFinalValueFromFile(2, fname, start, stop);
}

double OPFit::GetFinalValueFromFile(int pfromback, std::string fname,
                                    double start, double stop) {
  double pol_sum = 0.0;
  int nsteps = 0;
  double polarization = 0.0;
  double time = 0.0;
  std::ifstream ifs(fname, std::ifstream::in);
  if (!ifs.is_open()) {
    std::cout << "Could not open " << fname << std::endl;
    return 0.0;
  }
  std::string line;
  std::vector<std::string> word;
  while (std::getline(ifs, line)) {
    while (line.c_str()[0] == ' ') line.erase(0, 1);
    boost::split(word, line, boost::is_any_of(" \t"));
    try {
      polarization = stod(word[word.size() - pfromback]);
      time         = stod(word[0              ]);
      time = time*_us;
      if (time >= start && (time <= stop || stop < 0.0)) {
        // std::cout << "t = " << time/_us << " us, P = "
        //           << polarization << std::endl;
        pol_sum = pol_sum + polarization;
        nsteps++;
      }
    }
    catch (...) {
      std::cout << "Error converting to double in file " << fname << std::endl;
      // for (int i = 0; i < word.size(); i++) {
      //   std::cout << word[i] << std::endl;
      //      }
      // std::cout << word[0            ] << std::endl;
      // std::cout << word[word.size()-pfromback] << std::endl;      
      std::cout << line << std::endl;
    }
  }
  polarization = pol_sum / static_cast<double>(nsteps);
  //  std::cout << "Polarization = " << polarization << std::endl;
  return polarization;
}

int OPFit::AddTimingJitter(TH1D *model, double sigma, bool preserve_area,
                           double from_here, double to_here) {
  if (sigma < 1E-6) return 0;

  double initial_area = model -> Integral(model -> FindBin(from_here),
                                          model -> FindBin(to_here));
  TH1D *theclone = new TH1D(*model);

  TF1 *gaussfcn = new TF1("gauss", "gaus", model -> GetXaxis() -> GetXmin(),
                          model -> GetXaxis() -> GetXmax());
  gaussfcn -> SetParameter(2, sigma);   // parameter 2 is sigma
  for (int i = 1; i < model -> GetNbinsX(); i++) {
    double content = 0.0;
    // cout << "Considering bin " << i << " at " << model -> GetBinCenter(i)
    //      << endl;
    double time = model -> GetBinLowEdge(i);    
    if (ex_max > ex_min && time > ex_min && time < ex_max) {
      content = 0.0;
    } else {
      for (int b = 0; b < model -> GetNbinsX(); b++) {
        // parameter 0 is constant
        gaussfcn -> SetParameter(0, theclone -> GetBinContent(b));
        // parameter 1 is mean (middle)
        gaussfcn -> SetParameter(1, theclone -> GetXaxis() -> GetBinCenter(b));
        
        double contrib = gaussfcn -> Eval(model ->
                                          GetXaxis() -> GetBinCenter(i));
        // cout << "Contribution from bin " << b << " (center = "
        //      << gaussfcn -> GetParameter(1) << ", content = "
        //      << gaussfcn -> GetParameter(0) << ") is " << contrib << endl;
        content = content + contrib;
      }
    }
    //    cout << "Total bin content is " << content << endl;
    model -> SetBinContent(i, content);
  }

  double final_area = model -> Integral(model -> FindBin(from_here),
                                        model -> FindBin(to_here));
  if (preserve_area) {
    model -> Scale(initial_area/final_area);
  }

  delete theclone;
  delete gaussfcn;
  return 0;
}
