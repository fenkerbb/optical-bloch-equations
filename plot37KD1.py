#!/usr/bin/python

import ROOT as r
import sys
from array import array

mycol = (r.kBlue, r.kRed, r.kGreen+2, r.kMagenta+1, r.kYellow-3, r.kCyan-6,
         r.kAzure, r.kOrange, r.kViolet, r.kSpring, r.kPink, r.kTeal-7)

def GetNtuple(fname) :
    nt = r.TNtuple("opData", "opData",
                   "t:g0:g1:g2:f0:f1:f2:f3:f4:e0:e1:e2:e3:e4:e5:e6:e7:tot:pol:ali:exc")
    nt.ReadFile(fname)
    return nt

def GetNtupleBehr(op, ground, excited) :
    nt = r.TNtuple("opbehr", "opbehr", "t:exc:gr:pol:ali:fluor")
    nt.ReadFile(op)
    print "Read ", op

    gr = r.TNtuple("opbehrground", "opbehrground", "t:g0:g1:g2:f0:f1:f2:f3:f4")
    gr.ReadFile(ground)
    print "Read ", ground

    ex = r.TNtuple("opbehrexcited", "opbehrexcited",
                   "t:e0:e1:e2:e3:e4:e5:e6:e7")
    ex.ReadFile(excited)
    print "Read ", excited 

    nt.AddFriend(gr)
    nt.AddFriend(ex)
    return nt

def GetGStates(nt, time_str=':t') :
    gstate = []
    for i in range(3) :
        command = "g" + str(i) + time_str
        n = nt.Draw(command, "", "goff")
        gstate.append(r.TGraph(int(n), nt.GetV2(), nt.GetV1()))
        gstate[i].GetYaxis().SetRangeUser(0, 1)
        gstate[i].SetLineColor(mycol[i])
        gstate[i].SetLineStyle((i+1)%10)
        gstate[i].SetLineWidth(3)
        gstate[i].SetTitle("|F=1,Mf=" + str(i-1))

    
    return gstate

def GetFStates(nt, time_str=':t') :
    fstate = []
    for i in range(5) :
        command = "f" + str(i) + time_str
        n = nt.Draw(command, "", "goff")
        fstate.append(r.TGraph(int(n), nt.GetV2(), nt.GetV1()))
        fstate[i].GetYaxis().SetRangeUser(0, 1)
        fstate[i].SetLineColor(mycol[i])
        fstate[i].SetLineStyle((i+1)%10)
        fstate[i].SetLineWidth(3)
        fstate[i].SetTitle("|F=2,Mf=" + str(i-2))

    return fstate

def GetEStates(nt, time_str=':t') :
    estate = []
    for i in range(8) :
        command = "e" + str(i) + time_str
        n = nt.Draw(command, "", "goff")
        estate.append(r.TGraph(int(n), nt.GetV2(), nt.GetV1()))

    # F = 1
    for i in range(0, 3) :
        estate[i].SetTitle("|F=1,Mf=" + str(i-2))

    # F = 2
    for i in range(3, 8) :
        estate[i].SetTitle("|F=2,Mf=" + str(i-6))

    for i in range(len(estate)) :
        estate[i].GetYaxis().SetRangeUser(0, 1)
        estate[i].SetLineColor(mycol[i%len(mycol)])
        estate[i].SetLineStyle((i+1)%10)
        estate[i].SetLineWidth(3)
    
    return estate

def GetESum(nt, time_str=':t', scale = 1.0) :
    command = "exc*" + str(scale) + time_str
    n = nt.Draw(command, "", "goff")
    exc_total = r.TGraph(int(n), nt.GetV2(), nt.GetV1())
    exc_total.SetNameTitle("ES Population", "ES Population")
    exc_total.SetLineColor(mycol[0])
    exc_total.SetLineWidth(3)
    return exc_total

def GetPItotal(nt, time_str=':t', scale = 1.0) :
    estate = GetEStates(nt, time_str)
    pi_weights = [1.01642, 1, 0.983582, 0.967165, 0.983582, 1, 1.01642, 1.03284]
    pival = array('d', [0]*estate[0].GetN())
    for i in range(estate[0].GetN()) :
        t = 0.0
        for j in range(8) :
            t = t + pi_weights[j]*estate[j].GetY()[i]
        pival[i] = t
    pi = r.TGraph(estate[0].GetN(), estate[0].GetX(), pival)
    pi.SetNameTitle("Photoionization rate", "Photoionization rate")
    pi.SetLineColor(mycol[1])
    pi.SetLineWidth(3)
    return pi

def GetNucPol(nt, time_str=':t', scale = 1.0) :
    command = "pol*" + str(scale) + time_str
    n = nt.Draw(command, "", "goff")
    pol = r.TGraph(int(n), nt.GetV2(), nt.GetV1())
    pol.SetNameTitle("Polarization", "Polarization")
    pol.SetLineColor(mycol[0])
    pol.SetLineWidth(3)
    return pol

def plotWithLegend(state) :
    leg = r.TLegend(0.1, 0.7, 0.48, 0.9)

    for i in range(len(state)) :
        leg.AddEntry(state[i], state[i].GetTitle(), "L")
    state[0].Draw("AL")
    for i in range(1, len(state)) :
        state[i].Draw("LSAME")
    leg.Draw("SAME")
    r.gPad.Update()
    

def plot37KD1(myargs) :
    for i in range(len(myargs)) :
        print myargs[i]

    fname = "opData.dat"
    if (len(myargs) > 1) :
        
        fname = myargs[1]

    print "Opening ", fname
    nt = 0

    if (myargs[1] == "-behr") :
        if (len(myargs) < 5) :
            print "Must give three input files ",
            print "(opbehr.out, opbehrground.out, opbehrexcited.out)"
            exit(0)
        nt = GetNtupleBehr(myargs[2], myargs[3], myargs[4])
    else :
        nt = GetNtuple(fname)

    time_str = ":t"
    if (myargs[1] == "-behr") :
        time_str = ":t*1000000"

    
    gstate = GetGStates(nt, time_str)
    gcan = r.TCanvas()
    gcan.cd()
    plotWithLegend(gstate)
    
    fstate = GetFStates(nt, time_str)
    fcan = r.TCanvas()
    fcan.cd()
    plotWithLegend(fstate)

    ###### Log plot of F, G state ######
    fgcan = r.TCanvas()
    fgleg = r.TLegend(0.1, 0.7, 0.48, 0.9)
    gstate[0].SetLineColor(mycol[0])
    
    xax = gstate[0].GetXaxis()
    xax.SetTitle("Time [#mus]")
    xax.SetTitleSize(0.05)
    xax.SetLabelSize(0.05)

    yax = gstate[0].GetYaxis()
    yax.SetRangeUser(0.000001, 1.1)
    yax.SetTitle("Population")
    yax.SetTitleSize(0.05)
    yax.SetLabelSize(0.05)
    fgleg.AddEntry(gstate[0], gstate[0].GetTitle(), "L")
    gstate[0].Draw("AL")
    for i in range(1, 3) :
        gstate[i].SetLineColor(mycol[i])
        gstate[i].Draw("LSAME")
        fgleg.AddEntry(gstate[i], gstate[i].GetTitle(), "L")
    for i in range(5) :
        fstate[i].SetLineColor(mycol[i+3])
        fstate[i].Draw("LSAME")
        fgleg.AddEntry(fstate[i], fstate[i].GetTitle(), "L")
        fgleg.Draw("SAME")
        fgcan.SetLogx(1)
        fgcan.SetLogy(1)
    ######################################

    estate = GetEStates(nt, time_str)
    ecan = r.TCanvas()
    ecan.SetTitle("Excited States")
    ecan.cd()
    plotWithLegend(estate)


    exc_total_can = r.TCanvas()
    exc_total_can.SetTitle("ES Sum")
    exc_total_can.cd()
    exc_total = GetESum(nt, time_str)
    plotWithLegend([exc_total])
    

    

    pol_can = r.TCanvas()
    pol_can.SetTitle("Nuclear polarization")
    pol_can.cd()
    pol = GetNucPol(nt, time_str)
    plotWithLegend([pol])
    pol_can.Update()

    final_pol = pol.GetY()[pol.GetN()-1]
    print "Final polarization: ", final_pol

    thresh = [0.95, 0.99, 0.999]
    for z in range(len(thresh)) :
        pol_threshold = abs(thresh[z]*final_pol)
        reach_threshold = False;
        pp = 0
        while (not reach_threshold and pp < pol.GetN()) :
            temp = abs(float(pol.GetY()[pp]))
            if (temp >= pol_threshold) :
                print "Polarization at ", thresh[z]*100,
                print "%%: (", temp, 
                print ") at t = ", pol.GetX()[pp], " microseconds"
                reach_threshold = True;
                
            pp = pp+1
    # while (not reach_threshold) :
    #     temp = abs(pol.GetY()[pp])
    #     print temp
    #     if (temp >= pol_threshold) :
    #         print "Polarization at 95% (", pol.GetY()[pp], " at t = ",
    #         print pol.GetX()[pp] << " microseconds"
    #         reach_threshold = True;
    #         pp = pp + 1
    ##########################################################################
    # Fit function based on Ioana's report is a power law, use this when
    # trying to reproudce her parameter sweeps

    fit_min = exc_total.GetX()[0]
    fit_max = exc_total.GetX()[exc_total.GetN()-1]
    pow_law = r.TF1("PowerLaw", "([0]*TMath::Power([1], x)) + [2]", fit_min, fit_max)
    
    pow_law.SetParNames("Peak", "TimeConstant", "Tail")
    pow_law.SetRange(fit_min, fit_max)
    pow_law.SetParameter(0, 0.014)
    pow_law.SetParameter(1, 0.67)
    pow_law.SetParameter(2, 0.00002)
    pow_law.SetParLimits(0, 0.0, 1.0)
    pow_law.SetParLimits(1, 0.0, 1.0)
    pow_law.SetParLimits(2, 0.0, 1.0)
    # "W" Set all weights to 1; ignore error bars
    # "R" Use the Range specified in the function range
    # "Q" Quiet mode (minimum printing)
    # "N" Do not store the graphics function, do not draw
    # "0" Do not plot the result of the fit. By default the fitted function
    #                   is drawn unless the option "N" above is specified.
    exc_total.Fit(pow_law, "WRQ")

    tail = exc_total.GetY()[exc_total.GetN()-1]
    peak = exc_total.GetY()[r.TMath.LocMax(exc_total.GetN(), exc_total.GetY())]


    print "Tail E.S. population %g" % tail
    print "Peak E.S. population %g" % peak 
    tailpeak = 0.0
    if (peak > 0.0) :
        tailpeak = tail/peak

    print "Tail/Peak = %g" % tailpeak

    pi = GetPItotal(nt, time_str)

    cpi = r.TCanvas("Photoionization")
    cpi.cd()    
    plotWithLegend([exc_total, pi])

    pirat = array('d', [0]*pi.GetN())
    for i in range(pi.GetN()) :
        if (exc_total.GetY()[i] > 0.0) :
            pirat[i] = pi.GetY()[i]/exc_total.GetY()[i]
    piratio = r.TGraph(pi.GetN(), pi.GetX(), pirat)
    piratio.SetNameTitle("PI Xsection / 4P", "sigma/4p")
    crat = r.TCanvas("PI/4P")
    crat.cd()
    plotWithLegend([piratio])
        

    print "Power law fit of the form a*b^t + c..."
    a = pow_law.GetParameter(0)
    b = pow_law.GetParameter(1)
    c = pow_law.GetParameter(2)
    print "a = %g" % a
    print "b = %g" % pow_law.GetParameter(1)
    print "c = %g" % c
    print "Fit Tail/Peak = %g" % (c/a)
    ###########################################################################

    print "Final |2,2> population ",
    print fstate[4].GetY()[fstate[4].GetN()-1]
    print "Final |2,1> population ",
    print fstate[3].GetY()[fstate[3].GetN()-1]
    print "Final |2,2> population ",
    print gstate[2].GetY()[gstate[2].GetN()-1]
    #Output for convenient copy/paste##########################################
    # print "%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t" % (tail, peak, tailpeak,
    #                                             final_pol, a, c, b, c/a)

    
    raw_input("Enter to end")


if (__name__ == "__main__") :
    plot37KD1(sys.argv)



