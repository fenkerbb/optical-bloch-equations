#!/usr/bin/python

import Tkinter
import numpy

def blue_circle(canvas,x,y, r):
   id = canvas.create_oval(x-r,y-r,x+r,y+r, fill = "black")
   return id


pop = numpy.loadtxt("opData.dat")


top = Tkinter.Tk()
top.attributes("-alpha", 0.8)
line_length = 190
line_space = 30
sublevel_dy = 150
max_rad = (sublevel_dy - 20) / 2.0
speedup = 1/50.
border = 10
    
ygrf2 = 790
yexf2 = border + sublevel_dy + max_rad
ygrf1 = ygrf2 - sublevel_dy
yexf1 = yexf2 + sublevel_dy


window = Tkinter.Tk()
C = Tkinter.Canvas(top, bg = "blue", height = ygrf2 + border, width = 1200)

grf2 = [0, 0, 0, 0, 0]
crf2 = [0, 0, 0, 0, 0]

circle_center_x = []
circle_center_y = []
circle = []

begin = border + line_length + line_space
end = begin + line_length
for i in range(3) :
    circle_center_x.append((begin + end) / 2.0)
    circle_center_y.append(ygrf2 - (sublevel_dy/2.0))
    circle.append(blue_circle(C, circle_center_x[i],
                  circle_center_y[i], max_rad))
    C.create_line(begin, ygrf2, end, ygrf2, fill = "white", width = 5)
    begin = end + line_space
    end = begin + line_length


begin = border
end = begin + line_length
for i in range(5) :
    circle_center_x.append((begin + end) / 2.0)
    circle_center_y.append(ygrf1 - (sublevel_dy/2.0))
    circle.append(blue_circle(C, circle_center_x[i],
                              circle_center_y[i], max_rad))
    C.create_line(begin, ygrf1, end, ygrf1, fill = "white", width = 5)
    begin = end + line_space
    end = begin + line_length

begin = border + line_length + line_space
end = begin + line_length
for i in range(3) :
    circle_center_x.append((begin + end) / 2.0)
    circle_center_y.append(yexf1 - (sublevel_dy/2.0))
    circle.append(blue_circle(C, circle_center_x[i],
                                     circle_center_y[i], max_rad))
    C.create_line(begin, yexf1, end, yexf1, fill = "white", width = 5)
    begin = end + line_space
    end = begin + line_length

begin = border
end = begin + line_length
for i in range(5) :
    circle_center_x.append((begin + end) / 2.0)
    circle_center_y.append(yexf2 - (sublevel_dy/2.0))
    circle.append(blue_circle(C, circle_center_x[i],
                              circle_center_y[i], max_rad))
    C.create_line(begin, yexf2, end, yexf2, fill = "white", width = 5)
    begin = end + line_space
    end = begin + line_length

    
C.pack()
size = 1.0
rel = 1.0

print("Plotting until %d") % len(pop)
for j in range(len(pop)) :
    for i in range(len(circle)) :
        C.delete(circle[i])
        rel = pop[j*speedup][i+1]
        if (i > 7) :
            rel = rel * 100.
        circle[i] = blue_circle(C, circle_center_x[i],
                                circle_center_y[i], max_rad*rel)

    rel = rel*0.9
    C.after(1)
    C.update()
    
raw_input("Enter to end")
