* Calculates sigma+/sigma- optical pumping using the quantum mechanical density matrix formalism
* Specifically designed for TRINAT atom-trapping experiment although other applications are possible

### How do I get set up? ###

* Requires boost library
* Requires GSL library
* Requires OMP library
* Requires c++11 enabled
* After cloning repository: make a directory "build" and then simply `make`
* To test: `./opticalPumping -f Verify/verify.in` and then `diff opData.dat Verity/verify_GOOD.dat` should return no differences 
### Contribution guidelines ###

* Use this code however you want, but credit me in any publications.


### Who do I talk to? ###

* Benjamin Fenker (ben@fenker.org)